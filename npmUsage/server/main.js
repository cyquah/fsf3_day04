
// Defines a port to listen to
const PORT = 3000;

// A function that handles requests and sends a response
// Since no path was specified, this function would handle all client requests
app.use(function (req, res) {
    res.send('It Works!! Path Hit: ' + req.originalUrl);
});

// Starts the server on localhost (default)
app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT);
});